#pragma once

#include <cstddef>
#include <unordered_set>

#include "traceable_page_pointer.h"

namespace memory
{
   namespace detail
   {
      template <class BaseT>
      class traceable_resource : public BaseT
      {
         using base_type = BaseT;
         using self_type = traceable_resource<base_type>;
         using page_type = typename base_type::page_type;
         using base_pointer_type = typename base_type::pointer_type;

      public:
         class pointer
            : public base_pointer_type
         {
         public:
            pointer() = default;

            pointer(base_pointer_type const & ptr)
               : base_pointer_type(ptr)
            {
            }

            pointer(pointer const & that)
               : pointer(static_cast<base_pointer_type const &>(that))
            {

            }

            ~pointer()
            {
            }

            void trace() const
            {
               if (base_pointer_type::get_page() != nullptr)
               {
                  static_cast<traceable_resource<base_type>*>(
                     base_pointer_type::get_page()->get_buffer_resource())->trace(*this);
               }
            }

            void lose() const
            {
               if (base_pointer_type::get_page() != nullptr)
               {
                  static_cast<traceable_resource<base_type>*>(
                     base_pointer_type::get_page()->get_buffer_resource())->lose(*this);
               }
            }
         };

         using pointer_type = pointer;

      public:
         template <typename T>
         pointer_type allocate(int object_count);

         void detach_pointer()
         {
            for (auto& ref : m_external_ref)
            {
               const_cast<base_pointer_type*>(ref)->detach();
            }
         }

         void trace(const pointer_type &ptr);

         void lose(const pointer_type &ptr);

         void mark_reachable();

         void mark_reachable(const base_pointer_type& ref);

      private:
         std::unordered_set<const base_pointer_type*> m_external_ref;
      };

      template <class BaseT>
      template <typename T>
      inline typename traceable_resource<BaseT>::pointer_type
         traceable_resource<BaseT>::allocate(int object_count)
      {
         return base_type::template allocate<T>(object_count);
      }

      template <class BaseT>
      inline void traceable_resource<BaseT>::trace(const pointer_type &ptr)
      {
         if (auto page = find_page((std::byte const *) &ptr))
         {
            page.value()->associate(&ptr);
         }
         else
         {
            m_external_ref.insert(&ptr);
         }
      }

      template <class BaseT>
      inline void traceable_resource<BaseT>::lose(const pointer_type &ptr)
      {
         auto count = m_external_ref.erase(&ptr);
         if (count == 0)
         {
            if (auto page = find_page((std::byte const *) &ptr))
            {
               count = page.value()->dissociate(&ptr);
            }
         }

         assert(count != 0);
      }

      template <class BaseT>
      inline void traceable_resource<BaseT>::mark_reachable()
      {
         for (auto& ext : m_external_ref)
         {
            mark_reachable(*ext);
         }
      }

      template <class BaseT>
      inline void traceable_resource<BaseT>::mark_reachable(const base_pointer_type& ref)
      {
         if (ref == nullptr)
         {
            return;
         }

         auto p = static_cast<page_type*>(ref.get_page());
         auto info = p->get_allocation_info((std::byte*)ref.get_object());

         assert(info.get_state() == page_type::object_state::object
            || info.get_state() == page_type::object_state::subobject);

         for (auto& ir : p->get_internal_ref())
         {
            auto ir_info = p->get_allocation_info((std::byte*)ir);
            assert(ir_info.get_state() == page_type::object_state::object
               || ir_info.get_state() == page_type::object_state::subobject);
            if (info.get_object_offset() == ir_info.get_object_offset())
            {
               std::cout << "Ref found [object = " << (std::byte*)ref.get_object()
                  << " subobject = " << (std::byte*)ir << "]" << std::endl;
            }
         }
      }
   }
}
