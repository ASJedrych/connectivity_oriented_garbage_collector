#pragma once

#include <cstddef>
#include <memory>

#include "buffer_page_pointer.h"
#include "../3rdparty/bitset.h"

namespace memory {
   namespace detail {

      class buffer_resource;

      class buffer_page
      {
      public:
         using pointer_type = buffer_page_pointer;

         enum class object_state
         {
            none,
            object,
            subobject,
            foreign,
            zombie
         };

         class allocation_info
         {
         public:
            allocation_info() = default;

            allocation_info(object_state state)
               : allocation_info(state, 0)
            {}

            allocation_info(object_state state, std::size_t s)
               : m_state(state)
               , m_object_offset(s)
            {}

            ~allocation_info() = default;

         public:
            auto get_state() const noexcept
            {
               return m_state;
            }

            auto get_object_offset() const noexcept
            {
               return m_object_offset;
            }

         private:
            object_state m_state = object_state::none;
            std::size_t m_object_offset = 0;
         };

         class journal
         {
         public:
            journal(std::size_t min_page_size, std::size_t sector_size)
               : m_page_size(min_page_size % sector_size > 0 ? min_page_size + (sector_size - (min_page_size % sector_size)) : min_page_size)
               , m_sector_size(sector_size)
               , m_used_sector(m_page_size / m_sector_size, false)
               , m_allocation_sector(m_page_size / m_sector_size, false)
            {}

         public:
            auto get_sector(std::size_t offset) const noexcept
            {
               return offset / m_sector_size;
            }

            auto get_sector_count() const noexcept
            {
               return get_sector(m_page_size);
            }

            auto find_next_allocation_sector(std::size_t i) const noexcept
            {
               for (; i < get_sector_count(); ++i)
               {
                  if (m_allocation_sector.get(i))
                  {
                     break;
                  }
               }
               return i;
            }

            template <typename T>
            std::optional<std::size_t> allocate_region(int object_count, std::size_t alignment_offset)
            {
               const auto requested_sector_count = to_sector(sizeof(T)*object_count) + 1;
               auto found = find_free_region<T>(requested_sector_count, get_sector(alignment_offset));
               if (found)
               {
                  mark_used(*found, requested_sector_count);
                  found = get_offset(*found);
               }

               return found;
            }

            void deallocate_region(std::size_t allocation_offset)
            {
               auto s = get_sector(allocation_offset);

               assert(m_allocation_sector.get(s) == true && "attempt to deallocate - not at start of a valid allocation");
               assert(m_used_sector.get(s) == true && "attempt to deallocate - location not in use");

               m_allocation_sector.set(s, false);
               
               auto next = find_next_allocation_sector(s + 1);

               while (s < next && m_used_sector.get(s))
               {
                  m_used_sector.set(s, false);
                  ++s;
               }
            }

            auto get_page_size() const noexcept
            {
               return m_page_size;
            }

            auto get_sector_size() const noexcept
            {
               return m_sector_size;
            }

            auto to_sector(std::size_t x) const noexcept
            {
               return 1 + get_sector(x - 1);
            }

            auto get_offset(std::size_t x) const noexcept
            {
               return x * m_sector_size;
            }

            allocation_info get_allocation_info(std::size_t x) const noexcept
            {
               auto s = get_sector(x);

               if (!m_used_sector.get(s))
               {
                  return { object_state::zombie, 0 };
               }

               if (!m_allocation_sector.get(s))
               {
                  auto f = find_object_address(s);
                  return { object_state::subobject, f };
               }

               return { object_state::object, s };

            }

         private:
            void mark_used(std::size_t i, std::size_t requested_bytes)
            {
               m_used_sector.set(i, i + requested_bytes, true);
               m_allocation_sector.set(i, true);
            }

            template <typename T>
            std::optional<std::size_t> find_free_region(std::size_t requested_sector_count, std::size_t alignment_offset)
            {
               std::optional<std::size_t> result;

               const auto alignment = to_sector(alignof(T));
               const auto end = get_sector_count() - requested_sector_count;

               std::size_t i = alignment_offset;

               for (; i < end; i += alignment)
               {
                  std::size_t j = 0;

                  for (; j < requested_sector_count; ++j)
                  {
                     if (m_used_sector.get(i + j))
                     {
                        // set i to next untested sector
                        i += (j + 1 - alignment);
                        break;
                     }
                  }

                  if (j == requested_sector_count)
                  {
                     break;
                  }
               }

               if (i >= end)
               {
                  return result;
               }

               return i;
            }

            std::size_t find_object_address(std::size_t x) const noexcept
            {
               while (x > 0 && !m_allocation_sector.get(x--));
               
               return x;
            }

         private:
            std::size_t m_page_size;
            std::size_t m_sector_size;
            bitset      m_used_sector;
            bitset      m_allocation_sector;
         };

      public:
         buffer_page(buffer_resource* rsc, std::size_t min_page_size = 1000, std::size_t min_alloc = 4)
            : m_journal(min_page_size, min_alloc)
            , m_storage(std::make_unique<std::byte[]>(m_journal.get_page_size()))
            , m_resource(rsc)
         {}

      public:
         buffer_resource * get_buffer_resource() const;

         template <typename T>
         pointer_type allocate(int object_count);

         void deallocate(std::byte * ptr);

         bool contain(std::byte const* ptr) const noexcept;

         allocation_info get_allocation_info(std::byte const* ptr) const noexcept;

         std::byte const* begin() const noexcept;

         std::byte const* end() const noexcept;

      private:
         journal m_journal;
         const std::unique_ptr<std::byte[]> m_storage;
         buffer_resource* m_resource;
      };

      inline buffer_resource* buffer_page::get_buffer_resource() const
      {
         return m_resource;
      }

      template <typename T>
      inline buffer_page::pointer_type buffer_page::allocate(int object_count)
      {
         const auto requested_size = object_count * sizeof(T);
         void* buffer_start = m_storage.get();
         auto buffer_size = m_journal.get_page_size();

         if (std::align(alignof(T), requested_size, buffer_start, buffer_size) == nullptr)
         {
            return nullptr;
         }

         auto alignment_offset = reinterpret_cast<std::byte*>(buffer_start) - m_storage.get();

         auto offset = m_journal.allocate_region<T>(object_count, static_cast<std::size_t>(alignment_offset));
         if (!offset)
         {
            return nullptr;
         }

         return pointer_type(m_storage.get() + (*offset), this);
      }

      inline void buffer_page::deallocate(std::byte * ptr)
      {
         m_journal.deallocate_region(static_cast<std::size_t>(ptr - m_storage.get()));
      }

      inline bool buffer_page::contain(std::byte const* ptr) const noexcept
      {
         auto const cmp = std::less<>{};

         return !cmp(ptr, begin()) && cmp(ptr, end());
      }

      inline buffer_page::allocation_info buffer_page::get_allocation_info(std::byte const* ptr) const noexcept
      {
         if (!contain(ptr))
         {
            return allocation_info(object_state::foreign);
         }

         auto x = static_cast<std::size_t>(ptr - m_storage.get());
         return m_journal.get_allocation_info(x);
      }
      
      inline std::byte const* buffer_page::begin() const noexcept
      {
         return &m_storage[0];
      }

      inline std::byte const* buffer_page::end() const noexcept
      {
         return &m_storage[m_journal.get_page_size()];
      }
   }
}