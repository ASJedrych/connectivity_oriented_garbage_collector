#pragma once

#include <vector>

namespace memory {
   namespace detail {

      template <typename BaseT>
      class traceable_page
         : public BaseT
      {
         using base_type = BaseT;

      public:
         using base_pointer_type = typename base_type::pointer_type;
      
      public:
         using base_type::base_type;

      public:
         void associate(const base_pointer_type* ptr)
         {
            m_internal_ref.push_back(ptr);
         }

         std::size_t dissociate(const base_pointer_type* ptr)
         {
            std::size_t result = 0;
            auto found = std::find(m_internal_ref.rbegin(), m_internal_ref.rend(), ptr);
            if (found != m_internal_ref.rend())
            {
               *found = m_internal_ref.back();
               m_internal_ref.pop_back();
               result = 1;
            }

            return result;
         }

         const std::vector<const base_pointer_type*>& get_internal_ref() const noexcept
         {
            return m_internal_ref;
         }

      private:
         std::vector<const base_pointer_type*> m_internal_ref;
      };
   }
}