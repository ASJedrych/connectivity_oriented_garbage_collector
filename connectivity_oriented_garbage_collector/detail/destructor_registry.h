#pragma once

#include <memory>
#include <type_traits>
#include <vector>

namespace memory
{
   class destructor_registry
   {
      class destructor
      {
      public:
         using raw_ptr_type = const void*;
         using destroy_function_type = void(*)(raw_ptr_type);

      public:
         destructor(raw_ptr_type ptr, destroy_function_type fun)
            : m_this(ptr)
            , m_destructor(fun)
         { }

      public:
         raw_ptr_type m_this = nullptr;
         destroy_function_type m_destructor = nullptr;
      };
   
   public:
      destructor_registry() = default;
      ~destructor_registry() = default;

   public:
      template <typename T>
      void insert(T* ptr)
      {
         if (!std::is_trivially_destructible<T>::value)
         {
            m_destructors.emplace_back(
                  ptr, 
                  [](destructor::raw_ptr_type x) { static_cast<const T*>(x)->~T(); });
         }
      }

      void clear()
      {
         for (auto &d : m_destructors)
         {
            d.m_destructor(d.m_this);
#if defined(STD_HEAP)
            ::operator delete(const_cast<void*>(d.m_this));
#endif
         }

         m_destructors.clear();
      }

   private:
      std::vector<destructor> m_destructors;
   };
}

