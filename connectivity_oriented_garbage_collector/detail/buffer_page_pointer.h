#pragma once

#include <cassert>

namespace memory
{
   namespace detail
   {    
      class buffer_page;

      class buffer_page_pointer
      {
         // using self_type = buffer_page_pointer;

         friend bool operator==(buffer_page_pointer const & lhs, nullptr_t) noexcept;
      
         friend bool operator!=(buffer_page_pointer const & lhs, nullptr_t) noexcept;

      public:
         buffer_page_pointer() = default;

         buffer_page_pointer(nullptr_t);

         buffer_page_pointer(void* obj, buffer_page* pg);

         buffer_page_pointer(buffer_page_pointer const& that);

         ~buffer_page_pointer() = default;

      public:
         void* get_object() const noexcept;

         void detach() noexcept;

         auto get_page() const noexcept;

      private:
         int compare(buffer_page_pointer const & that) const noexcept;
          
      private:
         void * m_object = nullptr;
         buffer_page * m_page = nullptr;
      };

      inline buffer_page_pointer::buffer_page_pointer(nullptr_t)
         : buffer_page_pointer()
      {}

      inline buffer_page_pointer::buffer_page_pointer(void* obj, buffer_page* pg)
         : m_object(obj)
         , m_page(pg)
      {
         assert(m_page != nullptr);
      }

      inline buffer_page_pointer::buffer_page_pointer(buffer_page_pointer const& that)
         : buffer_page_pointer(that.m_object, that.m_page)
      { }

      inline void * buffer_page_pointer::get_object() const noexcept
      {
         return m_object;
      }

      inline void buffer_page_pointer::detach() noexcept
      {
         m_page = nullptr;
         m_object = nullptr;
      }

      inline auto buffer_page_pointer::get_page() const noexcept
      {
         return m_page;
      }

      inline int buffer_page_pointer::compare(buffer_page_pointer const& that) const noexcept
      {
         return get_object() < that.get_object() ? -1 : get_object() == that.get_object() ? 0 : 1;
      }

      inline bool operator==(buffer_page_pointer const & lhs, nullptr_t) noexcept
      {
         return lhs.compare(buffer_page_pointer(nullptr)) == 0;
      }

      inline bool operator!=(buffer_page_pointer const & lhs, nullptr_t) noexcept
      {
         return lhs.compare(buffer_page_pointer(nullptr)) != 0;
      }
   }
}
