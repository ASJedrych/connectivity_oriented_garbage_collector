#pragma once

#include "buffer_page_pointer.h"

#include <utility>


namespace memory
{
   namespace detail
   {
      class gc_heap_ptr
         : public buffer_page_pointer
      {
      public:
         using base_type = buffer_page_pointer;

      public:
         gc_heap_ptr(base_type&& base)
            : base_type(std::move(base))
         {}
      };
   }

}