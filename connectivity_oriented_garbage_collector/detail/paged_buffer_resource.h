#pragma once

#include <list>
#include <optional>

#include "buffer_page.h"
#include "buffer_page_pointer.h"

// Inspired by ChunkyPtr (Mastering the C++17 STL)
// see: https://github.com/PacktPublishing/Mastering-the-Cpp17-STL/blob/master/Chapter08/examples-ch8.cc
namespace memory {
   namespace detail {

      class buffer_resource
      {};

      template <typename PageT = buffer_page>
      class paged_buffer_resource
         : public buffer_resource
      {
      public:
         using page_type = PageT;
         using pointer_type = buffer_page_pointer;

      public:
         template <typename T>
         pointer_type allocate(int object_count);

      protected:
         std::optional<page_type*> find_page(std::byte const* ptr) noexcept;

      private:
         std::list<page_type> m_pages;
      };

      template <typename PageT>
      template <typename T>
      inline typename paged_buffer_resource<PageT>::pointer_type
         paged_buffer_resource<PageT>::allocate(int object_count)
      {
         for (auto&& p : m_pages)
         {
            if (auto ptr = p.allocate<T>(object_count); ptr != nullptr)
            {
               return ptr;
            }
         }

         m_pages.emplace_back(this);

         return m_pages.back().allocate<T>(object_count);
      }

      template <typename PageT>
      inline std::optional<typename paged_buffer_resource<PageT>::page_type*>
         paged_buffer_resource<PageT>::find_page(std::byte const * ptr) noexcept
      {
         std::optional<page_type*> result;
         for (auto&& p : m_pages)
         {
            if (p.contain(ptr))
            {
               result = &p;
               break;
            }
         }

         return result;
      }
   }
}