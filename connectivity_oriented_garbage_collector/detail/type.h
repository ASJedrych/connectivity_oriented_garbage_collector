#pragma once

#include "paged_buffer_resource.h"
#include "traceable_resource.h"
#include "traceable_page.h"

namespace memory {
   using page_type = detail::traceable_page<detail::buffer_page>;
   using buffer_resource_type = detail::traceable_resource<detail::paged_buffer_resource<page_type>>;
   using pointer_type = buffer_resource_type::pointer_type;
}
