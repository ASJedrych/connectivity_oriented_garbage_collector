#pragma once

#include <cassert>

#include "detail/type.h"

namespace memory
{
   class gc_heap;

   template <typename T>
   class gc_ptr
      : public pointer_type
   {
      friend gc_heap;

      template <typename U>
      friend bool operator==(gc_ptr<U> const & lhs, gc_ptr<U> const & rhs) noexcept;

      template <typename U>
      friend bool operator==(gc_ptr<U> const & lhs, nullptr_t) noexcept;

      template <typename U>
      friend bool operator==(nullptr_t, gc_ptr<U> const & rhs) noexcept;

      template <typename U>
      friend bool operator!=(gc_ptr<U> const & lhs, gc_ptr<U> const & rhs) noexcept;

      template <typename U>
      friend bool operator!=(gc_ptr<U> const & lhs, nullptr_t) noexcept;

      template <typename U>
      friend bool operator!=(nullptr_t, gc_ptr<U> const & rhs) noexcept;

      template <typename U>
      friend bool operator<(gc_ptr<U> const & lhs, gc_ptr<U> const & rhs) noexcept;

      template <typename U>
      friend bool operator<=(gc_ptr<U> const & lhs, gc_ptr<U> const & rhs) noexcept;

      template <typename U>
      friend bool operator>(gc_ptr<U> const & lhs, gc_ptr<U> const & rhs) noexcept;

      template <typename U>
      friend bool operator>=(gc_ptr<U> const & lhs, gc_ptr<U> const & rhs) noexcept;

   public:
      gc_ptr() = default;

      gc_ptr(std::nullptr_t);

      gc_ptr(gc_ptr<T> const& that);

      ~gc_ptr();

   private:

      gc_ptr(pointer_type && ptr);

   public:
      explicit operator bool() const noexcept;

      T & operator*() const noexcept;

      T * operator->() const noexcept;

      T * get() const noexcept;

   private:
      int compare(gc_ptr const & that) const noexcept;
   };

   template <typename T>
   gc_ptr<T>::gc_ptr(std::nullptr_t)
      : gc_ptr<T>::gc_ptr()
   {}


   template <typename T>
   inline gc_ptr<T>::gc_ptr(gc_ptr const & ptr)
      : pointer_type(ptr)
   {
      pointer_type::trace();
   }

   template <typename T>
   inline gc_ptr<T>::gc_ptr(pointer_type && ptr)
      : pointer_type(std::move(ptr))
   {
      pointer_type::trace();
   }

   template <typename T>
   inline gc_ptr<T>::~gc_ptr()
   {
      pointer_type::lose();
   }

   template <typename T>
   inline gc_ptr<T>::operator bool() const noexcept
   {
      return (get_object() != nullptr);
   }

   template<typename T>
   inline T & gc_ptr<T>::operator*() const noexcept
   {
      assert(get_object() != nullptr);
      return *get();
   }

   template<typename T>
   inline T * gc_ptr<T>::operator->() const noexcept
   {
      assert(get_object() != nullptr);
      return get();
   }

   template<typename T>
   inline T * gc_ptr<T>::get() const noexcept
   {
      return static_cast<T*>(get_object());
   }

   template<typename T>
   inline int gc_ptr<T>::compare(gc_ptr const & that) const noexcept
   {
      return get() < that.get() ? -1 : get() == that.get() ? 0 : 1;
   }

   template<typename U>
   inline bool operator==(gc_ptr<U> const & lhs, gc_ptr<U> const & rhs) noexcept
   {
      return lhs.compare(rhs) == 0;
   }

   template<typename U>
   inline bool operator==(gc_ptr<U> const & lhs, nullptr_t) noexcept
   {
      return lhs.compare(gc_ptr<U>(nullptr)) == 0;
   }

   template<typename U>
   inline bool operator==(nullptr_t, gc_ptr<U> const & rhs) noexcept
   {
      return gc_ptr<U>(nullptr).compare(rhs) == 0;
   }


   template<typename U>
   inline bool operator!=(gc_ptr<U> const & lhs, gc_ptr<U> const & rhs) noexcept
   {
      return lhs.compare(rhs) != 0;
   }

   template<typename U>
   inline bool operator!=(gc_ptr<U> const & lhs, nullptr_t) noexcept
   {
      return !(lhs == nullptr);
   }

   template<typename U>
   inline bool operator!=(nullptr_t, gc_ptr<U> const & rhs) noexcept
   {
      return !(nullptr == rhs);
   }

   template<typename U>
   inline bool operator<(gc_ptr<U> const & lhs, gc_ptr<U> const & rhs) noexcept
   {
      return lhs.compare(rhs) < 0;
   }

   template<typename U>
   inline bool operator<=(gc_ptr<U> const & lhs, gc_ptr<U> const & rhs) noexcept
   {
      return lhs.compare(rhs) <= 0;
   }

   template<typename U>
   inline bool operator>(gc_ptr<U> const & lhs, gc_ptr<U> const & rhs) noexcept
   {
      return lhs.compare(rhs) > 0;
   }

   template<typename U>
   inline bool operator>=(gc_ptr<U> const & lhs, gc_ptr<U> const & rhs) noexcept
   {
      return lhs.compare(rhs) >= 0;
   }
}
