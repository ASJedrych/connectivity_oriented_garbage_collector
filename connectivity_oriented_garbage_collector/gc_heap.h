#pragma once

#include "detail/destructor_registry.h"
#include "detail/gc_heap_ptr.h"
#include "detail/type.h"

#include "gc_ptr.h"

namespace memory
{
   class buffer_page;

   class gc_heap
   {
   public:
      gc_heap() = default;
      ~gc_heap();

   public:
      template <typename T, typename... Args>
      gc_ptr<T> make_gc(Args&&... args);

      void shrink_to_fit();

   private:
      template <typename T>
      pointer_type allocate();

      template <typename T, typename... Args>
      void construct(T* obj, Args&&... args);

   private:
      destructor_registry m_destructors;
      buffer_resource_type m_buffer_resource;
   };

   inline gc_heap::~gc_heap()
   {
      m_buffer_resource.detach_pointer();
      m_destructors.clear();
   }

   template <typename T, typename... Args>
   inline gc_ptr<T> gc_heap::make_gc(Args&&... args)
   {
      auto p = allocate<T>();
      if (p != nullptr)
      {
         construct<T>((T*)p.get_object(), std::forward<Args>(args)...);
      }

      return p;
   }

   inline void gc_heap::shrink_to_fit()
   {
      m_buffer_resource.mark_reachable();
   }

   template <typename T>
   inline pointer_type gc_heap::allocate()
   {
#if defined(STD_HEAP)
      void* ptr = ::operator new(sizeof(T));
#else
      pointer_type ptr = m_buffer_resource.allocate<T>(1);
#endif

      assert(ptr.get_object() != nullptr && "failed to allocate memory");

      return ptr;
   }

   template <typename T, typename... Args>
   inline void gc_heap::construct(T* obj, Args&&... args)
   {
      ::new (obj) T{ std::forward<Args>(args)... };

      m_destructors.insert(obj);
   }
}
