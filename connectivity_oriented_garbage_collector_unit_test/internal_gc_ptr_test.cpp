#include "pch.h"

#include "../connectivity_oriented_garbage_collector/gc_ptr.h"
#include "../connectivity_oriented_garbage_collector/gc_heap.h"

#include "../connectivity_oriented_garbage_collector/detail/paged_buffer_resource.h"

namespace memory
{
   namespace test
   {
      class udt_withref
      {
      public:
         udt_withref()
            : m_ref()
         {
            std::ostringstream os;
            os << ">> Ctor this = " << this << '\n';
            std::cout << os.str();
         }

         udt_withref(gc_ptr<udt_withref> ref)
            : m_ref(ref)
         {
            std::ostringstream os;
            os << ">> Ctor this = " << this << '\n';
            std::cout << os.str();
         }

         udt_withref(const udt_withref& that)
            : m_ref(that.m_ref)
         {
            std::ostringstream os;
            os << ">> Ctor this = " << this << '\n';

            std::cout << os.str();
         }

         ~udt_withref()
         {
            std::ostringstream os;
            os << "<< Dtor this = " << this << '\n';
            std::cout << os.str();
         }

         friend bool operator==(udt_withref const& lhs, udt_withref const & rhs)
         {
            return lhs.m_ref == rhs.m_ref;
         }

         friend bool operator!=(udt_withref const& lhs, udt_withref const & rhs)
         {
            return !(lhs == rhs);
         }

         gc_ptr<udt_withref> get_ref() const
         {
            return m_ref;
         }

      private:
         gc_ptr<udt_withref> m_ref;
      };


      template <typename T>
      class internal_gc_ptr_test
         : public ::testing::Test
      {
      public:
         using test_type = T;
      };

      using test_types =::testing::Types<udt_withref>;
      TYPED_TEST_CASE(internal_gc_ptr_test, test_types);

      TYPED_TEST(internal_gc_ptr_test, gc_ptr_heap_default_construction) {
         gc_heap r;
         gc_ptr<test_type> p = r.make_gc<test_type>();
         EXPECT_TRUE((bool)p);
         EXPECT_EQ(*p, test_type());
      }

      TYPED_TEST(internal_gc_ptr_test, gc_ptr_heap_non_default_construction) {
         gc_heap r;
         const gc_ptr<test_type> c_ref_val = r.make_gc<test_type>();
         gc_ptr<test_type> p = r.make_gc<test_type>(c_ref_val);
         EXPECT_TRUE((bool)p);
         EXPECT_EQ(p->get_ref(), c_ref_val);
      }

      TYPED_TEST(internal_gc_ptr_test, gc_ptr_rel_op) {
         gc_heap r;
         gc_ptr<test_type> q = r.make_gc<test_type>();
         EXPECT_FALSE(q == nullptr);
         EXPECT_FALSE(nullptr == q);
         EXPECT_TRUE(q != nullptr);
         EXPECT_TRUE(nullptr != q);
      }

      TYPED_TEST(internal_gc_ptr_test, gc_ptr_heap_mark) {
         gc_heap r;
         const gc_ptr<test_type> c_ref_val = r.make_gc<test_type>();
         gc_ptr<test_type> p = r.make_gc<test_type>(c_ref_val);

         r.shrink_to_fit();
      }



   }
}

