#include "pch.h"

#include "../connectivity_oriented_garbage_collector/gc_ptr.h"
#include "../connectivity_oriented_garbage_collector/gc_heap.h"

#include "../connectivity_oriented_garbage_collector/detail/paged_buffer_resource.h"

namespace memory
{
   namespace test
   {
      class udt_noref
      {
      public:
         udt_noref(int val)
            : m_val(val)
         {
            std::ostringstream os;
            os << ">> Ctor this = " << this << '\n';
            std::cout << os.str();
         }

         udt_noref()
            : udt_noref(0)
         {
         }

         udt_noref(const udt_noref& that)
            : m_val(that.m_val)
         {
            std::ostringstream os;
            os << ">> Ctor this = " << this << '\n';
            std::cout << os.str();
         }

         ~udt_noref()
         {
            std::ostringstream os;
            os << "<< Dtor this = " << this << '\n';
            std::cout << os.str();
         }

         int get() const
         {
            return m_val;
         }

         friend bool operator==(udt_noref const& lhs, udt_noref const & rhs)
         {
            return lhs.m_val == rhs.m_val;
         }

         friend bool operator!=(udt_noref const& lhs, udt_noref const & rhs)
         {
            return !(lhs == rhs);
         }

      private:
         int m_val;
      };

      template <typename T>
      class external_gc_ptr_test
         : public ::testing::Test
      {
      public:
         using test_type = T;
      };

      using  test_types =::testing::Types<int, long, udt_noref>;
      TYPED_TEST_CASE(external_gc_ptr_test, test_types);

      TYPED_TEST(external_gc_ptr_test, gc_ptr_heap_default_construction) {
         gc_heap r;
         gc_ptr<test_type> p = r.make_gc<test_type>();
         EXPECT_TRUE((bool)p);
         EXPECT_EQ(*p, test_type());
      }

      TYPED_TEST(external_gc_ptr_test, gc_ptr_heap_non_default_construction) {
         const test_type c_ref_val = 4;
         gc_heap r;
         gc_ptr<test_type> p = r.make_gc<test_type>(c_ref_val);
         EXPECT_TRUE((bool)p);
         EXPECT_EQ(*p, c_ref_val);
      }

      TYPED_TEST(external_gc_ptr_test, gc_ptr_rel_op) {
         gc_heap r;
         gc_ptr<test_type> q = r.make_gc<test_type>(0);
         EXPECT_FALSE(q == nullptr);
         EXPECT_FALSE(nullptr == q);
         EXPECT_TRUE(q != nullptr);
         EXPECT_TRUE(nullptr != q);
      }
   }
}
